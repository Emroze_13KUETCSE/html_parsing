package com.limited.umbrellatech.html_parsing;

import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.android.volley.AuthFailureError;
import com.android.volley.Cache;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import retrofit.Callback;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class Main2Activity extends AppCompatActivity {

    public static String PREFS_NAME = "Emmi";
    SharedPreferences preference;
    SharedPreferences.Editor editor;


    ArrayList<Item2> total_medicine = new ArrayList<>();

    int range=30000;//26088//3737//19758//14866
    int counter = 25001;//16187/ncl/10217
    int totalC=0;

    String baseUrl = "http://68.183.189.81/";

    //String company="innova-pharmaceuticals-limited",id="36";

    //String companyName = "Innova Pharmaceuticals Limited.";

    //3 finish
    boolean flag_ok = false, thread_flag = false;
    Thread t;
    int threadCounter = 0;

    int companyCounter=0;

    Button ok;

    int cc=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        preference = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        editor = preference.edit();

        new FindCompanyName().execute();
        //REST_API();
        //httpMedic();

        t = new Thread() {

            @Override
            public void run() {
                try {
                    while (!isInterrupted()) {

                        if (thread_flag) {
                            thread_flag = false;
                            REST_API(total_medicine.get(cc));
                        }


                        Thread.sleep(0,1);
                    }
                } catch (InterruptedException e) {
                }
            }
        };
        t.start();
        ok = findViewById(R.id.btn);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                httpMedic();
            }
        });
    }

    public void httpMedic(){

        String url = baseUrl+"medic/v2/medicines";



        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new com.android.volley.Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {

                        Log.d("CODE>>>>>>>>>>>>>>>>>"," =================  RESPONSE ====================    "+response);
                        try {
                            JSONObject object = new JSONObject(response);
                            boolean ss = object.getBoolean("success");
                            if(ss){
                                Log.d("CODE>>>>>>>>>>>>>>>>>"," =================  OK ====================    "+object.getJSONArray("medicines").length());
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        //Toast.makeText(emailAuth.this,response,Toast.LENGTH_LONG).show();
                    }
                },
                new com.android.volley.Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {



                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", "1234");
                return params;
            }

            /*@Override
            protected Map<String, String> getParams()
            {
                Map<String, String> params = new HashMap<String, String>();
                params.put("name", item.getMedicine());
                params.put("generic", item.getGeneric());
                params.put("quantity", item.getAmount());
                params.put("medicine_type", item.getType());
                params.put("company", item.getCompany());


                return params;
            }*/
            @Override
            protected com.android.volley.Response<String> parseNetworkResponse(NetworkResponse response) {
                try {
                    Cache.Entry cacheEntry = HttpHeaderParser.parseCacheHeaders(response);
                    if (cacheEntry == null) {
                        cacheEntry = new Cache.Entry();
                    }
                    final long cacheHitButRefreshed = 3 * 60 * 1000; // in 3 minutes cache will be hit, but also refreshed on background
                    final long cacheExpired = 24 * 60 * 60 * 1000; // in 24 hours this cache entry expires completely
                    long now = System.currentTimeMillis();
                    final long softExpire = now + cacheHitButRefreshed;
                    final long ttl = now + cacheExpired;
                    cacheEntry.data = response.data;
                    cacheEntry.softTtl = softExpire;
                    cacheEntry.ttl = ttl;
                    String headerValue;
                    headerValue = response.headers.get("Date");
                    if (headerValue != null) {
                        cacheEntry.serverDate = HttpHeaderParser.parseDateAsEpoch(headerValue);
                    }
                    headerValue = response.headers.get("Last-Modified");
                    if (headerValue != null) {
                        cacheEntry.lastModified = HttpHeaderParser.parseDateAsEpoch(headerValue);
                    }
                    cacheEntry.responseHeaders = response.headers;
                    final String jsonString = new String(response.data,
                            HttpHeaderParser.parseCharset(response.headers));
                    return com.android.volley.Response.success(jsonString, cacheEntry);
                } catch (UnsupportedEncodingException e) {
                    return com.android.volley.Response.error(new ParseError(e));
                }
            }

        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                100*1000,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        ));
        stringRequest.setShouldCache(true);

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    public void REST_API(Item2 item){
        RestAdapter adapter = new RestAdapter.Builder()
                .setEndpoint(baseUrl) //Setting the Root URL
                .setRequestInterceptor(new RequestInterceptor() {
                    @Override
                    public void intercept(RequestInterceptor.RequestFacade request) {
                        //request.addHeader("Accept", "application/json;versions=1");
                        request.addHeader("Authorization", "1234");
                    }
                })
                .build();

        AppConfig.insertDetails api = adapter.create(AppConfig.insertDetails.class);

        System.out.println(new Gson().toJson(item));
        String  uniqueID = UUID.randomUUID().toString();

        api.insertData(
                item.getMedicine(),
                item.getGeneric(),
                item.getAmount(),
                item.getType(),
                item.getCompany(),
                item.getPrice(),
                item.getPackPrice(),
                item.getIndications(),
                item.getTherapeutic_Class(),
                item.getPharmacology(),
                item.getDosage_Administration(),
                item.getInteraction(),
                item.getContraindications(),
                item.getSide_Effects(),
                item.getPregnancy_Lactation(),
                item.getPrecautions(),
                item.getOverdose_Effects(),
                item.getStorage_Conditions(),
                uniqueID,
                new Callback<retrofit.client.Response>() {
                    @Override
                    public void success(retrofit.client.Response result, retrofit.client.Response response) {

                        try {

                            BufferedReader reader = new BufferedReader(new InputStreamReader(result.getBody().in()));
                            String resp;
                            resp = reader.readLine();
                            Log.d("CODE>>>>>>>>>>>>>>>>>"," =================  RESPONSE ====================    "+resp);
                            JSONObject jObj = new JSONObject(resp);
                            boolean success = jObj.getBoolean("success");

                            if(success){
                                ++cc;
                                new FindCompanyName().execute();
                                Log.d("CODE>>>>>>>>>>>>>>>>>"," =================  OK ====================    "+cc);
                            }

                        } catch (IOException e) {
                            Log.d("Exception", e.toString());
                        } catch (JSONException e) {
                            Log.d("JsonException", e.toString());
                        }
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        Log.d("Exception", error.toString());
                        REST_API(total_medicine.get(cc));

                    }
                }
        );
    }

    class FindCompanyName extends AsyncTask<Void,Void,Void> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected Void doInBackground(Void... params) {
            Document doc = null;
            try {
                //doc = Jsoup.connect("https://medex.com.bd/companies/"+id+"/"+company+"?page="+String.valueOf(counter)).get();
                //https://medex.com.bd/brands?page=2
                doc = Jsoup.connect("https://medex.com.bd/brands/"+String.valueOf(counter)).get();
                int i = 0;
                Log.d("URL>>>>>>>>>>>>>>>>>  ", i+" "+"https://medex.com.bd/brands/"+String.valueOf(counter));


                Item2 item2 = new Item2();


                Elements elements2 = doc.select("span[style=margin-right: 6px;]");
                for (Element element : elements2) {
                    String name = element.text();
                    String medicine = "";
                    medicine = name;
                    //String[] separated = name.split(" Available Brand Names");
                    //String temp = separated[0]; // this will contain "Fruit"
                    //ccc = temp;
                    Log.d("CODE>>>>>>>>>>>>>>>>>  ", i+" "+name);
                    //companyList.add(temp);

                    ++i;
                    break;

                }

                Elements elementsMm = doc.select("title");
                for (Element element : elementsMm) {
                    String name = element.text();
                    String[] separated = name.split("\\|");
                    // this will contain "Fruit"
                    item2.setMedicine(separated[0].trim());
                    Log.d("Name >>>>>>>>>>>>>>>>>", " "+separated[0]);
                    Log.d("Name >>>>>>>>>>>>>>>>>", " "+name);

                    break;

                }

                i=0;
                Elements elements3 = doc.select("small[class=h1-subtitle]");
                for (Element element : elements3) {
                    String name = element.text();

                    /*String[] separated = medicine.split(" "+name);
                    medicine = separated[0]; // this will contain "Fruit"
                    //ccc = temp;
                    Log.d(">>>>>>>>>>>>>>>>>  ", i+" "+medicine);*/
                    Log.d("Type>>>>>>>>>>>>>>>>>  ", i+" "+name);
                    //item2.setMedicine(medicine);
                    item2.setType(name);

                    //companyList.add(temp);
                    ++i;

                }

                i=0;
                Elements elements4 = doc.select("div[title=Generic Name]");
                for (Element element : elements4) {
                    String name = element.text();

                    Log.d("GENERIC", i+" "+name);
                    if(!name.equals("")){
                        item2.setGeneric(name);
                    }


                    ++i;

                }

                i=0;
                Elements elements41 = doc.select("div[title=Manufactured by] a");
                for (Element element : elements41) {
                    String name = element.text();

                    Log.d("COMPANY", i+" "+name);
                    if(!name.equals("")){
                        item2.setCompany(name);
                    }

                    ++i;

                }

                i=0;
                Elements elements42 = doc.select("div[title=Strength]");
                for (Element element : elements42) {
                    String name = element.text();

                    Log.d("AMOUNT", i+" "+name);
                    if(!name.equals("")){
                        item2.setAmount(name);
                    }

                    ++i;

                }

                i=0;
                Elements elements62 = doc.select("div[class=package-container]");
                for (Element element : elements62) {
                    String name = element.text();
                    Log.d("PRICE", i+" "+name);

                    String[] separated = name.split(": ৳ ");

                    try{
                        String pp = separated[1];
                        Log.d("PRICE", i+" "+pp);
                        if(!pp.equals("")){
                            String[] separated2 = pp.split(" \\(");
                            Log.d("PRICE", separated2[0]);

                            item2.setPrice(separated2[0].trim());


                        }
                    }catch (Exception e){
                        item2.setPrice("0.0");

                    }


                    try{
                        String[] separated2 = name.split(" \\(");

                        String pack = separated2[1].replace(')',' ').trim();
                        Log.d("P PRICE", pack);

                        item2.setPackPrice(pack);
                    }catch (Exception e){
                        item2.setPackPrice("");
                    }
                    ++i;

                    break;
                }


                i=0;
                //Elements elements5 = doc.select("div[class=generic-data-container ac-body]");
                Elements elements5 = doc.getElementsByClass("generic-data-container en");

                for (Element element : elements5) {
                    //String name = element.text();

                    //String[] separated = name.split(" Available Brand Names");
                    //String temp = separated[0]; // this will contain "Fruit"
                    //ccc = temp;
                    //Log.d("CODE>>>>>>>>>>>>>>>>>  ", i+" "+name);
                    i=0;
                    Elements temp = element.getElementsByClass("ac-body");
                    for(Element e : temp){
                        String name = e.text();
                        //Log.d("CODE>>>>>>>>>>>>>>>>>  ", i+" "+name);
                        if(i == 0){
                            item2.setIndications(name);
                        }
                        else if(i == 1){
                            item2.setTherapeutic_Class(name);
                        }
                        else if(i == 2){
                            item2.setPharmacology(name);
                        }
                        else if(i == 3){
                            item2.setDosage_Administration(name);
                        }
                        else if(i == 4){
                            item2.setInteraction(name);
                        }
                        else if(i == 5){
                            item2.setContraindications(name);
                        }
                        else if(i == 6){
                            item2.setSide_Effects(name);

                        }
                        else if(i == 7){
                            item2.setPregnancy_Lactation(name);
                        }
                        else if(i == 8){
                            item2.setPrecautions(name);
                        }
                        else if(i == 9){
                            item2.setOverdose_Effects(name);
                        }
                        else if(i == 10){
                            item2.setStorage_Conditions(name);
                        }

                        //companyList.add(temp);
                        ++i;
                    }


                }

                Log.d("CODE>>>>>>>>>>>>>>>>>", "\nMedicine : "+item2.getMedicine()+"\nGeneric : "+item2.getGeneric()+"\nAmount : "+
                            item2.getAmount()+"\nType : "+item2.getType()+"\nCompany : "+item2.getCompany()
                            +"\nIndications : "+item2.getIndications()
                            +"\nTherapeutic Class : "+item2.getTherapeutic_Class()
                            +"\nPharmacology : "+item2.getPharmacology()
                            +"\nDosage & Administration : "+item2.getDosage_Administration()
                            +"\nInteraction : "+item2.getInteraction()
                            +"\nContraindications : "+item2.getContraindications()
                            +"\nSide Effects : "+item2.getSide_Effects()
                            +"\nPregnancy & Lactation : "+item2.getPregnancy_Lactation()
                            +"\nPrecautions : "+item2.getPrecautions()
                            +"\nOverdose Effects : "+item2.getOverdose_Effects()
                            +"\nStorage Conditions : "+item2.getStorage_Conditions()

                    );

                total_medicine.add(item2);

                //Log.d("CODE>>>>>>>>>>>>>>>>>"," =================  Type ==================== "+"\nStorage Conditions : "+item2.getStorage_Conditions());

                flag_ok = true;

            } catch (IOException e) {
                flag_ok = false;
                e.printStackTrace();
                Log.d("CODE>>>>>>>>>>>>>>>>>",""+e.toString());
            }
            editor.putString("count",String.valueOf(counter));
            editor.commit();
            Log.d("CODE>>>>>>>>>>>>>>>>>"," ================= COUNTER  ====================  "+ counter);
            ++counter;
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            if(counter <= range+1){
                if(flag_ok){
                    //REST_API(total_medicine.get(cc));
                    thread_flag = true;
                }
                else {
                    new FindCompanyName().execute();
                }

            }else {
                for(int a = 0 ; a < total_medicine.size(); a++){

                    /*Log.d("CODE>>>>>>>>>>>>>>>>>", a+"\nMedicine : "+total_medicine.get(a).getMedicine()+"\nGeneric : "+total_medicine.get(a).getGeneric()+"\nAmount : "+
                            total_medicine.get(a).getAmount()+"\nType : "+total_medicine.get(a).getType()+"\nCompany : "+total_medicine.get(a).getCompany()
                            +"\nIndications : "+total_medicine.get(a).getIndications()
                            +"\nTherapeutic Class : "+total_medicine.get(a).getTherapeutic_Class()
                            +"\nPharmacology : "+total_medicine.get(a).getPharmacology()
                            +"\nDosage & Administration : "+total_medicine.get(a).getDosage_Administration()
                            +"\nInteraction : "+total_medicine.get(a).getInteraction()
                            +"\nContraindications : "+total_medicine.get(a).getContraindications()
                            +"\nSide Effects : "+total_medicine.get(a).getSide_Effects()
                            +"\nPregnancy & Lactation : "+total_medicine.get(a).getPregnancy_Lactation()
                            +"\nPrecautions : "+total_medicine.get(a).getPrecautions()
                            +"\nOverdose Effects : "+total_medicine.get(a).getOverdose_Effects()
                            +"\nStorage Conditions : "+total_medicine.get(a).getStorage_Conditions()

                    );*/
                }



            }
        }
    }

}
