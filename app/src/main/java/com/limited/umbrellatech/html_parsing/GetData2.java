package com.limited.umbrellatech.html_parsing;

public class GetData2 {
    boolean success;
    Medicines medicines;

    public GetData2(boolean success, Medicines medicines) {
        this.success = success;
        this.medicines = medicines;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public Medicines getMedicines() {
        return medicines;
    }

    public void setMedicines(Medicines medicines) {
        this.medicines = medicines;
    }
}
