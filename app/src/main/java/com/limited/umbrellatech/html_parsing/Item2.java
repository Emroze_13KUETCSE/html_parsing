package com.limited.umbrellatech.html_parsing;

public class Item2 {
    String medicine="";
    String generic="";
    String type="";
    String amount="";
    String company="";
    String price = "";
    String packPrice="";
    String Indications="";
    String Therapeutic_Class="";
    String Pharmacology="";
    String Dosage_Administration="";
    String Interaction="";
    String Contraindications="";
    String Side_Effects="";
    String Pregnancy_Lactation="";
    String Precautions="";
    String Overdose_Effects="";
    String Storage_Conditions="";


    public Item2() {
    }


    public String getPackPrice() {
        return packPrice;
    }

    public void setPackPrice(String packPrice) {
        this.packPrice = packPrice;
    }

    public String getMedicine() {
        return medicine;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public void setMedicine(String medicine) {
        this.medicine = medicine;
    }

    public String getGeneric() {
        return generic;
    }

    public void setGeneric(String generic) {
        this.generic = generic;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getIndications() {
        return Indications;
    }

    public void setIndications(String indications) {
        Indications = indications;
    }

    public String getTherapeutic_Class() {
        return Therapeutic_Class;
    }

    public void setTherapeutic_Class(String therapeutic_Class) {
        Therapeutic_Class = therapeutic_Class;
    }

    public String getPharmacology() {
        return Pharmacology;
    }

    public void setPharmacology(String pharmacology) {
        Pharmacology = pharmacology;
    }

    public String getDosage_Administration() {
        return Dosage_Administration;
    }

    public void setDosage_Administration(String dosage_Administration) {
        Dosage_Administration = dosage_Administration;
    }

    public String getInteraction() {
        return Interaction;
    }

    public void setInteraction(String interaction) {
        Interaction = interaction;
    }

    public String getContraindications() {
        return Contraindications;
    }

    public void setContraindications(String contraindications) {
        Contraindications = contraindications;
    }

    public String getSide_Effects() {
        return Side_Effects;
    }

    public void setSide_Effects(String side_Effects) {
        Side_Effects = side_Effects;
    }

    public String getPregnancy_Lactation() {
        return Pregnancy_Lactation;
    }

    public void setPregnancy_Lactation(String pregnancy_Lactation) {
        Pregnancy_Lactation = pregnancy_Lactation;
    }

    public String getPrecautions() {
        return Precautions;
    }

    public void setPrecautions(String precautions) {
        Precautions = precautions;
    }

    public String getOverdose_Effects() {
        return Overdose_Effects;
    }

    public void setOverdose_Effects(String overdose_Effects) {
        Overdose_Effects = overdose_Effects;
    }

    public String getStorage_Conditions() {
        return Storage_Conditions;
    }

    public void setStorage_Conditions(String storage_Conditions) {
        Storage_Conditions = storage_Conditions;
    }
}
