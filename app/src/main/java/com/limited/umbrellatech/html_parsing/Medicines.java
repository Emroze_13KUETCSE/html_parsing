package com.limited.umbrellatech.html_parsing;

public class Medicines {

    String id;
    String name;
    String generic;
    String quantity;
    String medicine_type;
    String comapny;

    public Medicines(String id, String name, String generic, String quantity, String medicine_type, String comapny) {
        this.id = id;
        this.name = name;
        this.generic = generic;
        this.quantity = quantity;
        this.medicine_type = medicine_type;
        this.comapny = comapny;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGeneric() {
        return generic;
    }

    public void setGeneric(String generic) {
        this.generic = generic;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getMedicine_type() {
        return medicine_type;
    }

    public void setMedicine_type(String medicine_type) {
        this.medicine_type = medicine_type;
    }

    public String getComapny() {
        return comapny;
    }

    public void setComapny(String comapny) {
        this.comapny = comapny;
    }

    /*"id": 1,
            "name": "Abaclor ",
            "generic": "Cefaclor Monohydrate",
            "quantity": "250 mg",
            "medicine_type": "Capsule",
            "comapny": "ACI Limited."*/
}
