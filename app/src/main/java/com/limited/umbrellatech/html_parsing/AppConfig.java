package com.limited.umbrellatech.html_parsing;

/**
 * Created by emroz on 4/21/2017.
 */

import com.google.gson.JsonElement;

import java.util.List;

import retrofit.Callback;
import retrofit.client.Response;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.POST;


public class AppConfig {

    public interface insert {
        @FormUrlEncoded
        @POST("/medic/v2/add_medicine")
        void insertData(
                @Field("name") String regi,
                @Field("generic") String batch_time,
                @Field("quantity") String s_nick,
                @Field("medicine_type") String s_full,
                @Field("company") String father_name,

                Callback<Response> callback);
    }

    public interface insertDetails {
        @FormUrlEncoded
        @POST("/care-plus/v2/add_medicine_details")
        void insertData(
                @Field("name") String name,
                @Field("generic") String generic,
                @Field("quantity") String quantity,
                @Field("medicine_type") String medicine_type,
                @Field("company") String company,
                @Field("price") String price,
                @Field("pack_price") String packPrice,

                @Field("indications") String indications,
                @Field("therapeutic_class") String therapeutic_class,
                @Field("pharmacology") String pharmacology,
                @Field("dosage_administration") String dosage_administration,
                @Field("interaction") String interaction,
                @Field("contraindications") String contraindications,
                @Field("side_effects") String side_effects,
                @Field("pregnancy_lactation") String pregnancy_lactation,
                @Field("precautions") String precautions,
                @Field("overdose_effects") String overdose_effects,
                @Field("storage_conditions") String storage_conditions,
                @Field("guid") String guid,


                Callback<Response> callback);
    }

    public interface getMedicines {
        @GET("/medic/v2/medicines")
        void get(Callback<Response> callback);
    }



}
