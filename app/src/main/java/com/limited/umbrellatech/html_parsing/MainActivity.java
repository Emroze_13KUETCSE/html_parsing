package com.limited.umbrellatech.html_parsing;

import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Environment;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import retrofit.Callback;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.RetrofitError;


public class MainActivity extends AppCompatActivity {

    ArrayList<String> medicine = new ArrayList<>();
    ArrayList<String> type = new ArrayList<>();
    ArrayList<String> generic = new ArrayList<>();
    ArrayList<String> amount = new ArrayList<>();
    ArrayList<String> companyList = new ArrayList<>();
    ArrayList<String> companyLink = new ArrayList<>();



    ArrayList<Item> total_medicine = new ArrayList<>();

    int range=300;
    int counter = 1;
    int totalC=0;

    String baseUrl = "http://192.168.0.4:8888/";

    //String company="innova-pharmaceuticals-limited",id="36";

    //String companyName = "Innova Pharmaceuticals Limited.";

    //3 finish
    boolean flag_ok = false;
    Thread t;
    int threadCounter = 0;

    int companyCounter=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        new FindCompanyName().execute();

    }

    public void REST_API(Item item){
        RestAdapter adapter = new RestAdapter.Builder()
                .setEndpoint(baseUrl) //Setting the Root URL
                .setRequestInterceptor(new RequestInterceptor() {
                    @Override
                    public void intercept(RequestInterceptor.RequestFacade request) {
                        //request.addHeader("Accept", "application/json;versions=1");
                        request.addHeader("Authorization", "MMR*Jp6Mu_9wgkv_Nr9e9CC3_jQZU79ARB3^rBa6p4nLsB^cWqQXQZ");
                    }
                })
                .build();

        AppConfig.insert api = adapter.create(AppConfig.insert.class);

        api.insertData(
                item.getMedicine(),
                item.getGeneric(),
                item.getAmount(),
                item.getType(),
                item.getCompany(),
                new Callback<retrofit.client.Response>() {
                    @Override
                    public void success(retrofit.client.Response result, retrofit.client.Response response) {

                        try {

                            BufferedReader reader = new BufferedReader(new InputStreamReader(result.getBody().in()));
                            String resp;
                            resp = reader.readLine();
                            Log.d("CODE>>>>>>>>>>>>>>>>>"," =================  RESPONSE ====================    "+resp);
                            JSONObject jObj = new JSONObject(resp);
                            boolean success = jObj.getBoolean("success");

                            if(success){
                                ++threadCounter;
                                ++totalC;
                                flag_ok = true;
                                Log.d("CODE>>>>>>>>>>>>>>>>>"," =================  OK ====================    "+totalC);
                            }

                        } catch (IOException e) {
                            Log.d("Exception", e.toString());
                        } catch (JSONException e) {
                            Log.d("JsonException", e.toString());
                        }
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        Log.d("Exception", error.toString());

                    }
                }
        );
    }

    public void httpMedic(final Item item){


        String url = baseUrl+"medic/v2/add_medicine";

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {

                        Log.d("CODE>>>>>>>>>>>>>>>>>"," =================  RESPONSE ====================    "+response);
                        try {
                            JSONObject object = new JSONObject(response);
                            boolean ss = object.getBoolean("success");
                            if(ss){
                                ++threadCounter;
                                flag_ok = true;
                                Log.d("CODE>>>>>>>>>>>>>>>>>"," =================  OK ====================    "+threadCounter);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        //Toast.makeText(emailAuth.this,response,Toast.LENGTH_LONG).show();
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {



                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", "mW%y5g^md-L8PSt4Q3FQTC$4&XWvwVGz$m^75!GKpC9jN8dz");
                return params;
            }

            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String> params = new HashMap<String, String>();
                params.put("name", item.getMedicine());
                params.put("generic", item.getGeneric());
                params.put("quantity", item.getAmount());
                params.put("medicine_type", item.getType());
                params.put("company", item.getCompany());


                return params;
            }
            @Override
            protected Response<String> parseNetworkResponse(NetworkResponse response) {
                //statusCode = response.statusCode;
                return super.parseNetworkResponse(response);
            }

        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10*1000,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        ));

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    class HTTP extends AsyncTask<Void,Void,Void> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected Void doInBackground(Void... params) {

            final Item item = total_medicine.get(threadCounter);

            String url = baseUrl+"medic/v2/add_medicine";

            StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                    new Response.Listener<String>()
                    {
                        @Override
                        public void onResponse(String response) {

                            Log.d("CODE>>>>>>>>>>>>>>>>>"," =================  RESPONSE ====================    "+response);
                            try {
                                JSONObject object = new JSONObject(response);
                                boolean ss = object.getBoolean("success");
                                if(ss){
                                    ++threadCounter;
                                    if(threadCounter < total_medicine.size()){
                                        new HTTP().execute();
                                    }
                                    else {
                                        threadCounter=0;
                                        total_medicine.clear();
                                        new Parse().execute();
                                    }

                                    Log.d("CODE>>>>>>>>>>>>>>>>>"," =================  OK ====================    "+threadCounter);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            //Toast.makeText(emailAuth.this,response,Toast.LENGTH_LONG).show();
                        }
                    },
                    new Response.ErrorListener()
                    {
                        @Override
                        public void onErrorResponse(VolleyError error) {



                        }
                    }
            ) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("Authorization", "mW%y5g^md-L8PSt4Q3FQTC$4&XWvwVGz$m^75!GKpC9jN8dz");
                    return params;
                }

                @Override
                protected Map<String, String> getParams()
                {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("name", item.getMedicine());
                    params.put("generic", item.getGeneric());
                    params.put("quantity", item.getAmount());
                    params.put("medicine_type", item.getType());
                    params.put("company", item.getCompany());


                    return params;
                }
                @Override
                protected Response<String> parseNetworkResponse(NetworkResponse response) {
                    //statusCode = response.statusCode;
                    return super.parseNetworkResponse(response);
                }

            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                    10*1000,
                    0,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
            ));

            RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
            requestQueue.add(stringRequest);

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);


        }
    }


    class FindCompanyName extends AsyncTask<Void,Void,Void> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected Void doInBackground(Void... params) {
            Document doc = null;
            try {
                //doc = Jsoup.connect("https://medex.com.bd/companies/"+id+"/"+company+"?page="+String.valueOf(counter)).get();
                //https://medex.com.bd/brands?page=2
                doc = Jsoup.connect("https://medex.com.bd/companies/"+String.valueOf(counter)).get();
                int i = 0;

                String ccc = "";
                Elements elements2 = doc.select("h1[class=col-xs-12 page-heading-1]");
                for (Element element : elements2) {
                    String name = element.text();

                    String[] separated = name.split(" Available Brand Names");
                    String temp = separated[0]; // this will contain "Fruit"
                    ccc = temp;
                    Log.d("CODE>>>>>>>>>>>>>>>>>  ", i+" "+temp);
                    companyList.add(temp);
                    ++i;

                }

                Elements metaTags = doc.getElementsByTag("meta");


                i=0;
                for (Element metaTag : metaTags) {
                    String content = metaTag.attr("content");

                    if(i == 6){
                        companyLink.add(content);
                        Log.d("CODE>>>>>>>>>>>>>>>>>  ", i+" "+content);

                    }
                    ++i;

                }


                //Log.d("CODE>>>>>>>>>>>>>>>>>"," =================  Type ====================");




            } catch (IOException e) {
                e.printStackTrace();
                Log.d("CODE>>>>>>>>>>>>>>>>>"," =================   ====================  "+e.toString());
            }
            ++counter;

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            if(counter <= range){
                new FindCompanyName().execute();
            }else {
                counter=1;
                range = 45;
                for(int a = 0 ; a < companyList.size(); a++){

                    Log.d("CODE>>>>>>>>>>>>>>>>>  ", a+" "+companyList.get(a)+" >> "+companyLink.get(a)+" >> ");
                }

                t = new Thread() {

                    @Override
                    public void run() {
                        try {
                            while (!isInterrupted()) {

                                if (flag_ok) {
                                    flag_ok = false;
                                    if(threadCounter >= total_medicine.size()){
                                        flag_ok=false;
                                        threadCounter=0;
                                        counter=1;
                                        range=45;
                                        total_medicine.clear();
                                        new Parse().execute();
                                    }
                                    else {
                                        REST_API(total_medicine.get(threadCounter));
                                    }

                                }


                                Thread.sleep(0,1);
                            }
                        } catch (InterruptedException e) {
                        }
                    }
                };
                t.start();
                new Parse().execute();
            }
        }
    }

    class Parse extends AsyncTask<Void,Void,Void> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected Void doInBackground(Void... params) {
            Document doc = null;

            medicine.clear();
            type.clear();
            amount.clear();
            generic.clear();
            try {
                doc = Jsoup.connect(companyLink.get(companyCounter)+"?page="+String.valueOf(counter)).get();
                //https://medex.com.bd/brands?page=2
                //doc = Jsoup.connect("https://medex.com.bd/brands"+"?page="+String.valueOf(counter)).get();

                //Log.d("CODE>>>>>>>>>>>>>>>>>","TAG "+doc.title());
                Elements elements = doc.select("div[class=col-xs-12 data-row-top]");
                int i = 0;
                Log.d("CODE>>>>>>>>>>>>>>>>>"," =================  Medicine ====================");
                for (Element element : elements) {
                    String name = element.text();
                    ++i;
                    medicine.add(name);
                    //Log.d("CODE>>>>>>>>>>>>>>>>>  ", i+" "+element.text());
                    /*String[] WC = name.split("\\s+");
                    int len = WC.length;
                    if(len<8){

                    }*/

                }
                i=0;
                //Log.d("CODE>>>>>>>>>>>>>>>>>"," =================  Type ====================");
                Elements elements2 = doc.select("span[class=inline-dosage-form]");
                for (Element element : elements2) {
                    String name = element.text();
                    ++i;
                    type.add(name);
                    //Log.d("CODE>>>>>>>>>>>>>>>>>  ", i+" "+element.text());
                    /*String[] WC = name.split("\\s+");
                    int len = WC.length;
                    if(len<8){

                    }*/

                }
                i=0;
                //Log.d("CODE>>>>>>>>>>>>>>>>>"," =================  Amount ====================");
                Elements elements3 = doc.select("div[class=col-xs-12 data-row-strength]");
                for (Element element : elements3) {
                    String name = element.text();
                    ++i;
                    amount.add(name);
                    //Log.d("CODE>>>>>>>>>>>>>>>>>  ", i+" "+element.text());
                    /*String[] WC = name.split("\\s+");
                    int len = WC.length;
                    if(len<8){

                    }*/

                }
                i=0;
                Log.d("CODE>>>>>>>>>>>>>>>>>"," =================  Generic ====================");
                Elements elements4 = doc.select("div[class=col-xs-12]");
                for (Element element : elements4) {
                    String name = element.text();


                    String[] WC = name.split("\\s+");
                    int len = WC.length;
                    if(len<10){
                        ++i;
                        //Log.d("CODE>>>>>>>>>>>>>>>>>  ", i+" "+element.text());
                        generic.add(element.text());
                    }

                }

                for(int a = 0 ; a < medicine.size(); a++){
                    Item item = new Item();
                    String[] separated = medicine.get(a).split(type.get(a));
                    String temp = separated[0]; // this will contain "Fruit"
                    item.setMedicine(temp);
                    item.setType(type.get(a));
                    item.setAmount(amount.get(a));
                    item.setCompany(companyList.get(companyCounter));
                    try{
                        item.setGeneric(generic.get(a));
                    }catch (IndexOutOfBoundsException e){
                        item.setGeneric("N/A");
                    }

                    if(!medicine.get(a).isEmpty()){
                        total_medicine.add(item);
                    }


                }

            } catch (IOException e) {
                e.printStackTrace();
                Log.d("CODE>>>>>>>>>>>>>>>>>"," =================   ====================  "+e.toString());
            }
            ++counter;

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            if(counter <= range){
                new Parse().execute();
            }else {

                for(int a = 0 ; a < total_medicine.size(); a++){

                    Log.d("CODE>>>>>>>>>>>>>>>>>  ", a+" "+total_medicine.get(a).getMedicine()+" >> "+total_medicine.get(a).getGeneric()+" >> "+
                            total_medicine.get(a).getAmount()+" >> "+total_medicine.get(a).getType()+" >> "+total_medicine.get(a).getCompany());
                }

                if(companyCounter < companyLink.size()){
                    threadCounter=0;
                    flag_ok=true;
                }
                else {

                    Log.d("CODE>>>>>>>>>>>>>>>>>"," =================  FINISH ====================  ");
                    finish();

                }
                ++companyCounter;

                /*t = new Thread() {

                    @Override
                    public void run() {
                        try {
                            while (!isInterrupted()) {

                                if (flag_ok) {
                                    flag_ok = false;
                                    if(threadCounter >= total_medicine.size()){
                                        Log.d("CODE>>>>>>>>>>>>>>>>>"," =================  FINISH ==================== "+company);
                                        t.interrupt();
                                    }
                                    else {
                                        httpMedic(total_medicine.get(threadCounter));
                                    }

                                }


                                Thread.sleep(0,1);
                            }
                        } catch (InterruptedException e) {
                        }
                    }
                };*/
                //t.start();

            }
        }
    }
}
